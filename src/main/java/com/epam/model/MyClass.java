package com.epam.model;

import com.epam.annotation.MyAnnotation;

public class MyClass {
    @MyAnnotation(nameOfAnimal = "Cat")
    private String name;
    private int levelOfAngry;
    @MyAnnotation(color = "rude")
    private String color;
    private int age;

    private void getInfo(){
        System.out.println( "Name: "+name +"\nColor: "+ color+"\nLevel of angry: "+levelOfAngry+"\nAge:"+age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevelOfAngry() {
        return levelOfAngry;
    }

    public void setLevelOfAngry(int levelOfAngry) {
        this.levelOfAngry = levelOfAngry;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return  "name='" + name + '\'' +
                ", levelOfAngry=" + levelOfAngry +
                ", color='" + color + '\'' +
                ", age=" + age;
    }

    public void myMethod() {
        System.out.println("my method!");
    }
    public int[] myMethod(String a,int ...args){
        int[] intArray = new int[args.length];
        for(int i=0;i<args.length;i++) {
            intArray[i] = args[i]+i;
        }
        return intArray;
    }

    public String[] myMethod(String ... args) {
        String[] stringArrays = new String[args.length];
        for(int i=0;i<args.length;i++){
            stringArrays[i] = args[i]+i;
        }
        return stringArrays;
    }
}
