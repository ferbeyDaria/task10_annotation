package com.epam.view;

import com.epam.annotation.MyAnnotation;
import com.epam.model.MyClass;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Scanner input = new Scanner(System.in);

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Test field with annotation");
        menu.put("2", "2 - Print information about animal");
        menu.put("3", "3 - Test unknown object");
        menu.put("4", "4 - Test invoke my method");
        menu.put("Q", "Q - exit ");

    }

    public MyView() {
        methodMenu = new LinkedHashMap<>();
        setMenu();
        methodMenu.put("1", this::test1);
        methodMenu.put("2", this::print);
        methodMenu.put("3", this::test3);
        methodMenu.put("4", this::test4);
    }

    private void test1() {
        System.out.println("Field with annotation: ");
        Class clazz = MyClass.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation myField = field.getAnnotation(MyAnnotation.class);
                String name = myField.nameOfAnimal();
                String color = myField.color();
                int levelOfAngry = myField.levelOfAngry();
                System.out.println(" color " + color);
                System.out.println(" name " + name);
                System.out.println(" age " + levelOfAngry);
                System.out.println();
            }
        }
    }

    private void print() {
        System.out.println("Information about animal: ");
        MyClass myClass = new MyClass();
        inf(myClass);
    }

    void inf(Object obj) {
        try {
            Class clazz = obj.getClass();
            Method methodCall = clazz.getDeclaredMethod("getInfo");
            methodCall.setAccessible(true);
            methodCall.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void test3() {
        System.out.println("My unknow object");
        MyClass myclass = new MyClass();
        reflUnkonownObject(myclass);
    }

    void reflUnkonownObject(Object obj) {
        try {
            Class clazz = obj.getClass();
            System.out.println("The name of class is: " + clazz.getSimpleName());
            System.out.println("The name of class is: " + clazz.getName());

            Constructor constructor = clazz.getConstructor();
            System.out.println("The name of constructor: " + constructor.getName());

            System.out.println("The declared method of class are: ");
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                System.out.println(" " + method.getName());
            }
            System.out.println();

            System.out.println("The declared fields of class are: ");
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                System.out.println(" " + field.getName());
            }
            System.out.println();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void test4() {
        System.out.println("Invoke my method: ");
        MyClass myClass = new MyClass();
        Class clazz = myClass.getClass();
        try {
            Method methodCall = clazz.getDeclaredMethod("myMethod");
            methodCall.invoke(myClass);
        }catch (Exception e) {}
        String[] strs = {"Dog","Cat","Mouse"};
        try {
            Method methodCall = clazz.getDeclaredMethod("myMethod",new Class[] {String[].class});
            methodCall.setAccessible(true);
            String[] strs2 = (String[]) methodCall.invoke(myClass,strs);
            for(String str : strs2) {
                System.out.println(str);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
//        int[] strs = {1,2,3};
//        try {
//            Method method = clazz.getDeclaredMethod("myMethod", new Class[] {String.class,int[].class});
//            method.setAccessible(true);
//            int[] strs2 = (int[]) method.invoke(myClass,"Hi",strs);
//            for(int str:strs2){
//                System.out.println(str);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
      }

    //------------------------------------------------------------------------------------------------
    private void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String menuKey;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            menuKey = input.nextLine().toUpperCase();
            try {
                methodMenu.get(menuKey).print();
            } catch (Exception e) {
            }
        } while (!menuKey.equals("Q"));
    }
}
